class HumanPlayer
  attr_reader :name

  # h = hit, x = miss, s = undamaged ship
  DISPLAY_HASH = { s:" ", nil=>" ",h:"x", x:"o" }

  def initialize(name)
    @name = name
  end

  def display(board)
    first_row="   |"
    divider="----"
    i=0
    while i< board.grid[0].length
      first_row += " #{i+1} |"
      divider += "----"
      i+=1
    end
    puts first_row
    puts divider
    cur_row = ""
    board.grid.each_with_index do |row, idx|
      cur_row += " #{idx+1} |"
      row.each_with_index do |col, jdx|
        cur_row += " #{DISPLAY_HASH[board[[idx,jdx]]]} |"
      end
      puts cur_row
      puts divider
      cur_row = ""
    end
  end

  def get_play
    puts "What position would you like to hit?"
    player_response = gets.chomp.split(",")
    row = player_response.first.to_i
    col = player_response.last.to_i
    [row-1,col-1]
  end
end

class ComputerPlayer
  attr_reader :name, :board

  def display(board)
    @board = board
  end

  def get_play
    success = 0
    raise "Board is full" if @board.full?
    while success == 0
      pos = random_position
      success = 1 if @board.valid_move?
    end
    pos
  end

  def random_position
    [rand(0...@board.grid.length), rand(0...@board.grid[0].length)]
  end

  def valid_move?(pos)
    valid_pos = [:s, nil]
    return true if valid_pos.includes?(self[pos])
    false
  end

end
