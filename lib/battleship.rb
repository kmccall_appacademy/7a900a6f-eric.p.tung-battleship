class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
  end

  def current_player
    @current_player
  end

  def switch_players!
    if @current_player == @player_one
      @current_player = @player_two
    else
      @current_player = @player_one
    end
  end

  def attack(pos)
    @board.place_mark(pos,:x) if @board.empty?(pos)
    @board.place_mark(pos,:h) if @board[pos]==:s
  end

  def count
    @board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    # @player.display(@board)
    pos = @player.get_play
    attack(pos)
  end

  def play
    until game_over?
      @player.display(@board)
      play_turn
    end
  end
end
