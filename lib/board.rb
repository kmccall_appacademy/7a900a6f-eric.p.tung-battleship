require 'byebug'

class Board
  attr_accessor :grid
  # h = hit, m = miss, s = undamaged ship
  DISPLAY_HASH = { s:" ", nil:" ",h:"x", m:"o" }

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    width = 10
    height = 10
    @grid = Array.new(height){ Array.new(width) }
  end

  def [](pos)
    row,col=pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row,col=pos
    @grid[row][col] = mark
  end

  def count
    counter_hash = Hash.new(0)
    @grid.flatten.each { |i| counter_hash[i]+=1 }
    counter_hash[:s]
  end

  def empty?(pos = nil)
    if pos.nil?
      (self.count==0 ? true : false)
    else
      row,col=pos
      self[[row,col]].nil?
    end
  end

  def full?
    return false if @grid.flatten.any? { |i| i.nil? }
    true
  end

  def place_mark(pos, mark)
    row, col = pos
    self[[row, col]] = mark
  end

  def random_position
    [rand(0...@grid.length), rand(0...@grid[0].length)]
  end

  def place_random_ship
    success = 0
    raise "Board is full" if self.full?
    while success == 0
      pos = random_position
      if empty?(pos)
        place_mark(pos, :s)
        success = 1
      end
    end
  end

  def won?
    return false if @grid.flatten.any? {|i| i==:s }
    true
  end

end
